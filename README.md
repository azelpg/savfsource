# savfsource

http://azsky2.html.xdomain.jp/

VapourSynth 用の映像読み込みプラグイン。<br>
ffmpeg の libavformat 等のライブラリを使って、余分な処理はせず、シンプルにフレームを読み込みます。<br>

ffms2 (ver 2.40 時点) で、WebM (VP9) の映像が正しく読み込めなかったので作りました。<br>
ffmpeg が読み込める動画は大体読み込めますが、フォーマット変換などの処理はしないので、ffms2 でうまく読み込めない動画で、プログレッシブかつ YUV フォーマットの場合のみ使ってください。

- 入力から読み込んだイメージをそのまま渡します。
- YUV フォーマットのみ対応。
- 可変フレームレートは考慮しません。

MP4 (H.264)、MKV (H.264)、WebM (VP9) で問題ないのを確認済み。<br>
WMV は終端に数枚の余分なフレームが入ります。

## 動作環境

- Linux、ほか Unix 系 OS
- VapourSynth R56 以降 (API4 を使います)

## ビルド・インストール

ninja コマンドが必要です。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~

## 使い方

デフォルトで、/usr/local/lib/vapoursynth にインストールされます。<br>
プラグインは自動で読み込まれないので、スクリプト内で手動で読み込んでください。

読み込み時にフォーマットは変換できないので、読み込んだ後にフォーマットを変換したい場合は、リサイズ関数を使ってください。

~~~
import vapoursynth as vs

core = vs.core

core.std.LoadPlugin("/usr/local/lib/vapoursynth/libsavfsource.so")

# 最初の映像トラックを読み込む
clip = core.savf.Source("src.mp4")

# 2番目の映像トラックを読み込む
#clip = core.savf.Source("src.mp4", track=2)

# フォーマットを変換する
#clip = core.resize.Point(clip,format=vs.YUV444P8)

clip.set_output()
~~~

## 関数

~~~
core.savf.Source(string source="filename", [int track=1])

source: 動画のファイル名。
track: 映像のトラック番号。先頭を1として、何番目の映像を使うか。
~~~
