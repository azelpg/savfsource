/*$
savfsource
Copyright (c) 2023 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#include <stdint.h>
#include <stdlib.h>
#include <VapourSynth4.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>

//------------

typedef struct
{
	int stream_index,
		depth,
		fpsnum,
		fpsden,
		tbase_num,
		tbase_den,
		next_frame,		//次のフレーム位置
		last_frame,		//最後に読み込んだフレーム位置 (dts から)
		frame_eof;		//1 でもうフレームがない

	AVFormatContext *fctx;
	AVCodecContext *cctx;
	AVFrame *frame;
	AVPacket *packet;

	VSVideoInfo vinfo;
}plugin_data;

//-------------


/* フレーム位置からタイムスタンプ計算 */

static int64_t _frame_to_ts(plugin_data *p,int n)
{
	return (int64_t)((double)n * p->tbase_den / p->tbase_num * p->fpsden / p->fpsnum + 0.5);
}

/* タイムスタンプからフレーム位置計算 */

static int _ts_to_frame(plugin_data *p,int64_t n)
{
	return (int)((double)n * p->tbase_num / p->tbase_den * p->fpsnum / p->fpsden + 0.5);
}

/* フレームイメージを取得
 *
 * return: -1 でさらに入力が必要 */

static int _decode_frame(plugin_data *p,AVPacket *packet)
{
	int ret;

	//デコーダに送る (packet = NULL でフラッシュ)

	avcodec_send_packet(p->cctx, packet);

	//取得

	ret = avcodec_receive_frame(p->cctx, p->frame);

	if(ret == AVERROR(EAGAIN))
		//入力が足りない
		ret = -1;
	else if(ret < 0 && ret != AVERROR_EOF)
		//エラー
		ret = 1;
	else
	{
		//成功 (EOF の場合、もうフレームがない)

		p->frame_eof = (ret == AVERROR_EOF);

		if(packet)
			p->last_frame = _ts_to_frame(p, packet->dts);
		else
			//フラッシュ後の場合
			p->last_frame++;

		ret = 0;
	}

	//

	if(packet) av_packet_unref(packet);

	return ret;
}

/* 指定位置のフレーム取得
 *
 * return: 0 で成功 */

static int _get_frame_av(plugin_data *p,int n)
{
	AVPacket *packet;
	int ret;

	//シーク (キーフレーム位置)

	if(p->next_frame != n)
	{
		av_seek_frame(p->fctx, p->stream_index, _frame_to_ts(p, n), AVSEEK_FLAG_BACKWARD);

		avcodec_flush_buffers(p->cctx);
	}

	//フレーム読み込み

	packet = p->packet;

	while(1)
	{
		//圧縮されたデータを読み込み
		
		if(av_read_frame(p->fctx, packet)) break;

		//ストリーム位置
	
		if(packet->stream_index != p->stream_index)
		{
			av_packet_unref(packet);
			continue;
		}

		//フレーム取得

		ret = _decode_frame(p, packet);

		if(ret == -1)
			continue;
		else if(ret)
			return 1;

		//指定フレーム位置まで来た
	
		if(n <= p->last_frame)
		{
			p->next_frame = n + 1;
			return 0;
		}

		//まだ来ていない場合は、次のフレームへ

		av_frame_unref(p->frame);
	}

	//終端まで読み込んだらフラッシュして、残りフレームを取得

	while(1)
	{
		if(_decode_frame(p, NULL)) return 1;

		if(n <= p->last_frame)
		{
			p->next_frame = n + 1;
			return 0;
		}

		av_frame_unref(p->frame);
	}

	return 0;
}

/* イメージを VSFrame にセット */

static void _set_image(plugin_data *p,VSFrame *frame,AVFrame *avf,const VSAPI *vsapi)
{
	uint8_t *dst,*src;
	int i,dpitch,spitch,size,iy,height;

	for(i = 0; i < 3; i++)
	{
		src = avf->data[i];
		spitch = avf->linesize[i];

		dst = vsapi->getWritePtr(frame, i);
		dpitch = vsapi->getStride(frame, i);
		height = vsapi->getFrameHeight(frame, i);

		if(p->frame_eof)
		{
			//フレームがないため、クリア
			// :8bit の場合のみ黒画面になる。それ以外はゼロクリア
			
			memset(dst, (i && p->depth == 8)? 0x80: 0, dpitch * height);
		}
		else if(spitch == dpitch)
			//全体をコピー
			memcpy(dst, src, spitch * height);
		else
		{
			//余白があるため、Y ごとにコピー
			
			size = spitch;
			if(dpitch < size) size = dpitch;

			for(iy = height; iy; iy--)
			{
				memcpy(dst, src, size);

				dst += dpitch;
				src += spitch;
			}
		}
	}

	//AVFrame 解放

	if(!p->frame_eof) av_frame_unref(avf);
}

/** (VS) フレーム取得関数 */

static const VSFrame *VS_CC _vs_getframe(int n,int activationReason,void *instanceData,
	void **frame_data,VSFrameContext *frame_ctx,VSCore *core,const VSAPI *vsapi)
{
	plugin_data *p = (plugin_data *)instanceData;
	VSFrame *frame;
	VSMap *props;
	AVFrame *avf;
	char pixtype;

	if(activationReason != arInitial) return NULL;

	//av フレーム取得

	if(_get_frame_av(p, n))
	{
		vsapi->setFilterError("failed get_frame", frame_ctx);
		return NULL;
	}

	avf = p->frame;

	//VSFrame 作成
	
	frame = vsapi->newVideoFrame(&p->vinfo.format, p->vinfo.width, p->vinfo.height, NULL, core);

	//イメージセット

	_set_image(p, frame, avf, vsapi);
	
	//プロパティ

	props = vsapi->getFramePropertiesRW(frame);

	vsapi->mapSetInt(props, "_SARNum", 1, maReplace);
	vsapi->mapSetInt(props, "_SARDen", 1, maReplace);
	vsapi->mapSetInt(props, "_DurationNum", p->fpsnum, maReplace);
	vsapi->mapSetInt(props, "_DurationDen", p->fpsden, maReplace);
	vsapi->mapSetFloat(props, "_AbsoluteTime", (double)n * p->fpsden / p->fpsnum, maReplace);

	if(avf->color_range != AVCOL_RANGE_UNSPECIFIED)
		vsapi->mapSetInt(props, "_ColorRange", (avf->color_range == AVCOL_RANGE_MPEG), maReplace);

	vsapi->mapSetInt(props, "_Matrix", avf->colorspace, maReplace);
	vsapi->mapSetInt(props, "_Primaries", avf->color_primaries, maReplace);
	vsapi->mapSetInt(props, "_Transfer", avf->color_trc, maReplace);

	if(avf->chroma_location > 0)
		vsapi->mapSetInt(props, "_ChromaLocation", avf->chroma_location - 1, maReplace);

	pixtype = av_get_picture_type_char(avf->pict_type);
	vsapi->mapSetData(props, "_PictType", &pixtype, 1, dtUtf8, maReplace);

	vsapi->mapSetInt(props, "_FieldBased", 0, maReplace);

	return frame;
}

/** ファイルを開く
 *
 * return: NULL で成功。失敗時はエラーメッセージ */

static const char *_open_source(VSCore *core,const VSAPI *vsapi,plugin_data *dat,const char *source,int track)
{
	AVFormatContext *fctx = NULL;
	AVCodecContext *cctx = NULL;
	AVStream **ppstrm,*strm;
	const AVCodec *codec;
	const AVPixFmtDescriptor *desc;
	int i,ctype,vcur;

	//開く

	if(avformat_open_input(&fctx, source, NULL, NULL) != 0)
		return "failed avformat_open_input";

	dat->fctx = fctx;

	//最初のデータから情報取得

	if(avformat_find_stream_info(fctx, NULL) < 0)
		return "failed avformat_find_stream_info";

	//指定トラック以外を無効に

	ppstrm = fctx->streams;
	vcur = 0;
	strm = NULL;

	for(i = 0; i < fctx->nb_streams; i++, ppstrm++)
	{
		ctype = (*ppstrm)->codecpar->codec_type;

		if(ctype == AVMEDIA_TYPE_VIDEO) vcur++;
	
		if(ctype == AVMEDIA_TYPE_VIDEO && vcur == track)
		{
			//見つかった
			strm = *ppstrm;
			dat->stream_index = i;
		}
		else
			//無効
			(*ppstrm)->discard = AVDISCARD_ALL;
	}

	//ストリームがない

	if(!strm) return "unfound video track";

	//codec

	codec = avcodec_find_decoder(strm->codecpar->codec_id);
	if(!codec) return "failed avcodec_find_decoder";

	dat->cctx = cctx = avcodec_alloc_context3(codec);
	if(!cctx) return "failed avcodec_alloc_context3";

	avcodec_parameters_to_context(cctx, strm->codecpar);

	if(avcodec_open2(cctx, codec, NULL) < 0)
		return "failed avcodec_open2";

	//YUV フォーマットのみ

	i = cctx->pix_fmt;

	if(i != AV_PIX_FMT_YUV420P && i != AV_PIX_FMT_YUV444P && i != AV_PIX_FMT_YUV422P && i != AV_PIX_FMT_YUV411P)
		return "pixel format is only YUV";

	//データ

	desc = av_pix_fmt_desc_get(cctx->pix_fmt);

	dat->fpsnum = strm->r_frame_rate.num;
	dat->fpsden = strm->r_frame_rate.den;
	dat->tbase_num = strm->time_base.num;
	dat->tbase_den = strm->time_base.den;
	dat->depth = desc->comp[0].depth; //Y のビット数
	dat->next_frame = 0;
	dat->last_frame = -1;

	dat->frame = av_frame_alloc();
	dat->packet = av_packet_alloc();

	//---- VSVideoInfo

	dat->vinfo.fpsNum = dat->fpsnum;
	dat->vinfo.fpsDen = dat->fpsden;
	dat->vinfo.width = cctx->width;
	dat->vinfo.height = cctx->height;

	//フレーム数

	if(strm->nb_frames)
		dat->vinfo.numFrames = strm->nb_frames;
	else if(strm->duration != AV_NOPTS_VALUE)
		//ストリームの時間
		dat->vinfo.numFrames = _ts_to_frame(dat, strm->duration);
	else
		//AVFormatContext::duration (1秒 = AV_TIME_BASE)
		dat->vinfo.numFrames = (int)((double)fctx->duration / AV_TIME_BASE * dat->fpsnum / dat->fpsden + 0.5);

	//ビデオ情報

	vsapi->queryVideoFormat(&dat->vinfo.format,
		cfYUV, stInteger, desc->comp[0].depth, desc->log2_chroma_w, desc->log2_chroma_h, core);

	return NULL;
}

/** (VS) Source の解放 */

static void VS_CC _vs_source_free(void *instance,VSCore *core,const VSAPI *vsapi)
{
	plugin_data *p = (plugin_data *)instance;

	if(p->frame) av_frame_free(&p->frame);
	if(p->packet) av_packet_free(&p->packet);

	if(p->cctx) avcodec_free_context(&p->cctx);

	if(p->fctx) avformat_close_input(&p->fctx);

	free(p);
}

/** (VS) Source が呼ばれた時の作成 */

static void VS_CC _vs_createsource(const VSMap *in,VSMap *out,void *userdat,VSCore *core,const VSAPI *vsapi)
{
	plugin_data *dat;
	VSNode *node;
	const char *source,*errmes;
	int err,track;

	//引数の値を取得

	source = vsapi->mapGetData(in, "source", 0, NULL);

	track = vsapi->mapGetIntSaturated(in, "track", 0, &err);
	if(err) track = 1;
	if(track <= 0)
	{
		vsapi->mapSetError(out, "Source: invalid 'track'");
		return;
	}
	
	//データ確保

	dat = (plugin_data *)calloc(1, sizeof(plugin_data));
	if(!dat)
	{
		vsapi->mapSetError(out, "failed alloc");
		return;
	}

	//開く

	errmes = _open_source(core, vsapi, dat, source, track);
	if(errmes)
	{
		_vs_source_free(dat, 0, 0);
		vsapi->mapSetError(out, errmes);
		return;
	}

	//ビデオフィルタ作成

	node = vsapi->createVideoFilter2("Source", &dat->vinfo,
		_vs_getframe, _vs_source_free, fmUnordered, NULL, 0, dat, core);

	vsapi->mapConsumeNode(out, "clip", node, maAppend);
}

/** VapourSynth プラグインの初期化関数 */

VS_EXTERNAL_API(void) VapourSynthPluginInit2(VSPlugin *plugin,const VSPLUGINAPI *vspapi)
{
	vspapi->configPlugin("com.vapoursynth.savfsource",
		"savf", //name space
		"savfsource", //表示用の名前
		VS_MAKE_VERSION(1,0), VAPOURSYNTH_API_VERSION, 0, plugin);

	//関数登録

	vspapi->registerFunction("Source",
		"source:data;track:int:opt;", "clip:vnode;", _vs_createsource, NULL, plugin);
}
